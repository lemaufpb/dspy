# DSPY - Data Science using Python

**Projeto Criado por**: 

- Aléssio Almeida (alessio@lema.ufpb.br)
- Hilton Martins (hilton@lema.ufpb.br)

Este projeto disponibiliza um conjunto de dados para subsidiar aulas aplicadas com análise de dados, bem como disponibiliza notebooks com introdução a linguagem Python direcionado para o propósito de análise de dados.

## Requisitos para o projeto

- `Python 3`: https://www.python.org/downloads/ 
- `Visual Studio Code` (VS Code): https://code.visualstudio.com/download 
- `PIP` (Python Installs Packages): https://pip.pypa.io/en/stable/installation/ 
- `Jupyter Notebook`, no terminal (cmd) digite: `pip install jupyter`
- `Git`: https://git-scm.com/download/win 

**Softwares extras**
- Mendeley: https://www.mendeley.com/download-reference-manager/windows
- Overleaf: https://www.overleaf.com
- Miktex: https://miktex.org/download 
- Texstudio: https://www.texstudio.org/ 

## 1. Requirements
Crie um arquivo `requirements.txt` na raiz do projeto com a lista de bibliotecas necessárias para sua execução, como por exemplo:
```
pandas==1.4.4
jupyter==1.0.0
streamlit==1.12.2
plotly==5.10.0
matplotlib==3.5.3
pymongo==3.11.4
scikit-learn==1.0.2
scipy==1.7.3
numpy==1.19.5
openpyxl==3.0.0
```

Outra forma, é deixar para criar os requirements ao final do desenvolvimento do projeto, de modo a congelar as versões dos pacotes que garantem a execução do projeto. Dessa forma, você pode ficar instalando individualmente cada biblioteca (`pip install nome-biblioteca`) e ao final você cria os requisitos por meio do seguinte comando:

```
pip freeze > requirements.txt
```

## 2. Ambiente virtual (virtual enviroment - venv)
Recomenda-se a criação de um ambiente virtual na raiz do projeto.

A ferramenta de ambiente virtual cria uma pasta dentro do diretório do projeto (por padrão, ela é chamada de venv). Quando o ambiente virtual é ativado, os pacotes instalados posteriormente são instalados dentro da pasta do ambiente virtual específico daquele projeto.

### Usuários Windows (Git Bash)
```
# Ambiente virtual
pip install virtualenv
python -m venv venv
source venv/Scripts/activate

# Instalando os pacotes requeridos
pip install -r requirements.txt
```

### Usuários MacOS
```
# Ambiente virtual
pip3 install virtualenv
python3 -m venv venv
source venv/bin/activate

# Instalando os pacotes requeridos: após ativar o ambiente basta usar 'python' e 'pip'

pip install --upgrade pip
pip install -r requirements.txt
```

Com o comando `pip install -r requirements.txt`, todas as bibliotecas necessárias serão instaladas no ambiente virtual (caso ele esteja ativo) ou no ambiente local (caso o venv esteja inativo).

Para desativar o venv basta apenas digitar no terminal: `deactivate`

## Controle de versionamento
- Criar um repositório remoto do projeto (GitLab ou GitHub)
- Clonar o projeto em um diretório local do computador

Caso o `VENV` esteja ativo no projeto, é aconselhável ignorar o diretório para fins de versionamento no `git`.

No arquivo `.gitignore`, insira nome de arquivos e pastas que não serão versionadas

```
.DS_Store
tests
.venv
venv
*venv
*.venv
*/.DS_Store
~$*.xls*
```


